import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'airports',
    pathMatch: 'full'
  },
  {
    path: 'airports',
    loadChildren: () => import('./airports-module/airports.module').then(take => take.AirportsModule)
  },
  {
    path: 'departures/:icao',
    loadChildren: () => import('./departures-module/departures.module').then(take => take.DeparturesModule)
  },
  {
    path: 'arrivals/:icao',
    loadChildren: () => import('./arrivals-module/arrivals.module').then(take => take.ArrivalsModule)
  },
  {
    path: 'map',
    loadChildren: () => import('./google-map-module/google-map.module').then(take => take.GoogleMapModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
