import {NgModule} from '@angular/core';

import {ArrivalsComponent} from './arrivals.component';
import {ArrivalsRouterModule} from "./arrivals.router-module";
import {MatTableModule} from "@angular/material/table";
import {CommonModule, NgIf, UpperCasePipe} from "@angular/common";
import {SharedModule} from "../shared-module/shared.module";
import {SchedulesService} from "../shared-module/services/schedules.service";
import {HttpClientModule} from "@angular/common/http";
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  imports: [ArrivalsRouterModule, MatTableModule, CommonModule, SharedModule, UpperCasePipe, HttpClientModule, MatIconModule],
  exports: [],
  declarations: [ArrivalsComponent],
  providers: [SchedulesService],
})
export class ArrivalsModule {
}
