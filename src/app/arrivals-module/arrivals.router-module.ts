import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ArrivalsComponent} from "./arrivals.component";

const routes: Routes = [
  {
    path: '',
    component: ArrivalsComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [],
  declarations: [],
})
export class ArrivalsRouterModule {
}
