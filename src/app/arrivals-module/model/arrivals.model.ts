
export interface Arrival {
  airline_icao: string;
  flight_number: string;
  arr_actual: string;
  arr_time: string;
  arr_icao: string;
  arr_gate: string | null;
  status: string;
  duration: number;
}
