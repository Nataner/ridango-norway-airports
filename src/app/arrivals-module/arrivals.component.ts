import  {Component, OnDestroy, OnInit } from '@angular/core';
import { filter, map, Observable, Subject, switchMap, takeUntil, tap } from "rxjs";
import { SchedulesService } from "../shared-module/services/schedules.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CdkTableDataSourceInput } from "@angular/cdk/table";
import { Location } from "@angular/common";
import { Arrival } from "./model/arrivals.model";

@Component({
  selector: 'arrivals',
  templateUrl: 'arrivals.component.html',
  styleUrls: ['arrivals.component.scss']
})

export class ArrivalsComponent implements OnInit, OnDestroy {

  public displayedColumns: string[] = ['time', 'destination', 'flight', 'duration', 'gate', 'remarks'];
  public dataSource: CdkTableDataSourceInput<Arrival> = [];
  private readonly _requiredFields: string[] = ['airline_icao', 'flight_number', 'arr_actual', 'arr_time', 'arr_icao', 'arr_gate', 'status', 'duration', 'arr_estimated']
  private _icao: string = '';
  private destroyed$: Subject<void> = new Subject<void>();
  constructor(
    private _scheduleService: SchedulesService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _location: Location
  ) {}

  ngOnInit() {
    this._getAirportArrivals();
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  public goBack = () => {
    this._location.back()
  }

  public goToDepartures = () => {
    void this._router.navigate([`/departures/${this._icao}`]);
  }

  public goToAirports = () => {
    void this._router.navigate(['/airports'])
  }

  private _getAirportArrivals(): void {
    this._route.params
      .pipe(
        map((params) => params['icao']),
        filter(Boolean),
        tap((icao) => this._icao = icao),

        switchMap((icao_code) => this._arrivals$(icao_code)),

        tap((arrivals) => this.dataSource = arrivals),
        takeUntil(this.destroyed$)
      )
      .subscribe();
  }

  private _arrivals$(icao_code: string): Observable<Arrival[]> {
    return this._scheduleService.arrivals(icao_code, { _fields: this._requiredFields })
  }
}
