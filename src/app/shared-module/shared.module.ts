import {NgModule} from '@angular/core';

import { MaterialModule } from "./angular-material-module/angular-material.module";
import { HeaderComponent } from "./components/header/header.component";
import { NavigationCard } from "./components/navigation-card/navigation-card";
import { UpperCasePipe } from "@angular/common";
import { HoursPipe } from "./pipes/hours.pipe";
import {NumberToTimePipe} from "./pipes/number-to-time/number-to-time.pipe";

@NgModule({
  imports: [MaterialModule, UpperCasePipe],
  exports: [
    HeaderComponent,
    NavigationCard,
    HoursPipe,
    NumberToTimePipe
  ],
  declarations: [HeaderComponent, NavigationCard, HoursPipe, NumberToTimePipe],
})
export class SharedModule {}
