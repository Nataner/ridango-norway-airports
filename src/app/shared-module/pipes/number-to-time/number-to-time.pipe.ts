import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'numberToTime'
})
export class NumberToTimePipe implements PipeTransform {
  transform(value: number): string {
    const hours: number = Math.floor(value / 60);
    let minutes: string = (value % 60).toString();
    if (minutes.length < 2) minutes = '0' + minutes
    return `${hours}:${minutes}`;
  }
}
