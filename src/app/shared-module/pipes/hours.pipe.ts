import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'hours'
})
export class HoursPipe implements PipeTransform {
  transform(value: string): string {
    return new Date(value).getHours() + `:${new Date(value).getMinutes()}`;
  }
}
