import {Component, Input, OnInit} from '@angular/core';
import {Location} from "@angular/common";
import {Router} from "@angular/router";

@Component({
  selector: 'navigation-card',
  templateUrl: 'navigation-card.html',
  styleUrls: ['navigation-card.scss']
})

export class NavigationCard implements OnInit {

  @Input() title!: string;
  @Input() icon!: string;
  @Input() navigationFunction!: () => void;
  constructor(
  ) {
  }

  ngOnInit() {}

  navigate() {
    this.navigationFunction();
  }
}
