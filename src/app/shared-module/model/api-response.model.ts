
export interface API_Response {
  request: Record<string, any>;
  response: any[],
  terms: string;
}
