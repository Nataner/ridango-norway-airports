import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map, Observable } from "rxjs";
import { API_Response } from "../model/api-response.model";
import {Departure} from "../../departures-module/model/departures.model";
import {Arrival} from "../../arrivals-module/model/arrivals.model";

@Injectable()
export class SchedulesService {

  private readonly _httpEndpoint: string = 'https://airlabs.co/api/v9/schedules?api_key=';
  private readonly _apiKey: string = '45758287-b699-4bad-9e9a-738caf07330d';
  constructor(private _http: HttpClient) {}

  arrivals(arr_icao: string,...arrivalsApiQueryParams: any[]): Observable<Arrival[]> {
    const { _fields  } = arrivalsApiQueryParams[0];
    const hasFields = !_fields ? '' : `&_fields=${_fields.toString()}`;
    return this._http.get<API_Response>(`${this._httpEndpoint}${this._apiKey}&arr_icao=${arr_icao}${hasFields}`, {})
      .pipe(
        map(response => response.response),
      )
  }
  departures(dep_icao: string,...departuresApiQueryParams: any[]): Observable<Departure[]> {
    const { _fields  } = departuresApiQueryParams[0];
    const hasFields = !_fields ? '' : `&_fields=${_fields.toString()}`;
    return this._http.get<API_Response>(`${this._httpEndpoint}${this._apiKey}&dep_icao=${dep_icao}${hasFields}`, {})
      .pipe(
        map(response => response.response),
      )
  }
}
