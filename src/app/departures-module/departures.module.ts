import {NgModule} from '@angular/core';

import { DeparturesComponent } from "./departures.component";
import {DeparturesRoutingModule} from "./departures.routing-module";
import {MatTableModule} from "@angular/material/table";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared-module/shared.module";
import {SchedulesService} from "../shared-module/services/schedules.service";
import {HttpClientModule} from "@angular/common/http";
import {MatIconModule} from "@angular/material/icon";


@NgModule({
  imports: [DeparturesRoutingModule, MatTableModule, SharedModule, CommonModule, HttpClientModule, MatIconModule],
  exports: [],
  declarations: [DeparturesComponent],
  providers: [SchedulesService],
})
export class DeparturesModule {
}
