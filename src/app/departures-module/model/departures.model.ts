
export interface Departure {
  airline_icao: string;
  flight_number: string;
  dep_actual: string;
  dep_time: string;
  dep_icao: string;
  gate: string;
  status: string;
  duration: number;
}
