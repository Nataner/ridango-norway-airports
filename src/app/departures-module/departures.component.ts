import { Component, OnDestroy, OnInit } from '@angular/core';
import { CdkTableDataSourceInput } from "@angular/cdk/table";
import { Departure } from "./model/departures.model";
import { SchedulesService } from "../shared-module/services/schedules.service";
import { filter, map, Observable, Subject, switchMap, takeUntil, tap } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";

@Component({
  selector: 'ng-departures',
  templateUrl: 'departures.component.html',
  styleUrls: ['departures.component.scss']
})

export class DeparturesComponent implements OnInit, OnDestroy {

  public displayedColumns: string[] = ['time', 'destination', 'flight', 'duration', 'gate', 'remarks'];
  public dataSource: CdkTableDataSourceInput<Departure> = [];
  private _icao: string = '';
  private _destroyed$: Subject<void> = new Subject<void>();
  private _requiredFields: string[] = ['airline_icao', 'flight_number', 'dep_actual', 'dep_time', 'dep_icao', 'status', 'gate', 'duration'];

  constructor(
    private _scheduleService: SchedulesService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _location: Location
  ) {}

  ngOnInit() {
    this._getAirportDepartures();
  }

  ngOnDestroy() {
    this._destroyed$.next();
    this._destroyed$.complete();
  }

  public goBack = () => {
    this._location.back();
  }

  public goToArrivals = () => {
    void this._router.navigate([`/arrivals/${this._icao}`]);
  }

  public goToAirports = () => {
    void this._router.navigate(['/airports'])
  }

  private _getAirportDepartures() {
    this._route.params
      .pipe(
        map(params => params['icao']),
        filter(Boolean),
        tap((icao) => this._icao = icao),

        switchMap((icao_code) => this._departures$(icao_code)),

        tap((departures) => this.dataSource = departures),
        takeUntil(this._destroyed$)
      )
      .subscribe()
  }

  private _departures$(icao_code: string): Observable<Departure[]> {
    return this._scheduleService.departures(icao_code, { _fields: this._requiredFields })
  }
}
