import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map, Observable } from "rxjs";
import { Airport } from "../model/airports.model";
import { API_Response } from "../../shared-module/model/api-response.model";

@Injectable({
  providedIn: 'root'
})
export class AirportsServices {

  constructor(private _http: HttpClient) {}

  private readonly _httpEndpoint: string = 'https://airlabs.co/api/v9/airports';
  private readonly _apiKey: string = '45758287-b699-4bad-9e9a-738caf07330d';

  /*
  * Sends request to airport data base via air labs API
  *
  * @Param {string} iata_code
  */
  public airports(): Observable<Airport[]> {
    return this._http.get<API_Response>(`${this._httpEndpoint}?api_key=${this._apiKey}&country_code=NO`)
      .pipe(
        map((response) => response.response)
      );
  }
}
