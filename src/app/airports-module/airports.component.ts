import { Component, OnInit } from '@angular/core';
import { AirportsServices } from "./services/airports.services";
import { catchError, filter, map, of, tap, throwIfEmpty } from "rxjs";
import { CdkTableDataSourceInput } from "@angular/cdk/table";
import { Airport } from "./model/airports.model";
import { SchedulesService } from "../shared-module/services/schedules.service";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";


@Component({
  selector: 'ng-airports',
  templateUrl: 'airports.component.html',
  styleUrls: ['airports.component.scss'],
})
export class AirportsComponent implements OnInit {

  private readonly _departureFields: string[] = ['airline_icao', 'flight_number', 'dep_actual', 'dep_time', 'dep_icao', 'status', 'gate', 'duration'];
  private readonly _arrivalsFields: string[] = ['airline_icao', 'flight_number', 'arr_actual', 'arr_time', 'arr_icao', 'arr_gate', 'status', 'duration', 'arr_estimated']
  public dataSource: CdkTableDataSourceInput<Airport> = [];
  public readonly displayedColumns: string[] = ['names', 'view_departures', 'view_arrivals'];
  constructor(
    private _airportsService: AirportsServices,
    private _schedulesService: SchedulesService,
    private _snackbar: MatSnackBar,
    private _router: Router
  ) {
  }

  ngOnInit() {
    this._getAirports();
  }

  public goToMap = () => {
    void this._router.navigate(['/map'])
  }

  public loadDepartures(icao_code: string): void {
    this._schedulesService.departures(icao_code, { _fields: this._departureFields })
      .pipe(
        throwIfEmpty(),
        tap((departures) => !departures.length && this._snackbar.open('There is no departures for this airport today','', { duration: 1000 })),
        filter((departures) => !!departures.length),
        tap(() => void this._router.navigate([`/departures/${icao_code.toLowerCase()}`])),
        catchError(() => {
          this._snackbar.open('Error from server try again later', '', { duration: 1000 });
          return of(null);
        })
      )
      .subscribe()
  }

  public loadArrivals(icao_code: string): void {
    this._schedulesService.arrivals(icao_code, { _fields: this._arrivalsFields })
      .pipe(
        throwIfEmpty(),
        tap((arrivals) => !arrivals.length && this._snackbar.open('There is no arrivals for this airport today','', { duration: 1000 })),
        filter((arrivals) => !!arrivals.length),
        tap(() => void this._router.navigate([`/arrivals/${icao_code.toLowerCase()}`])),
        catchError(() => {
          this._snackbar.open('Error from server try again later', '', { duration: 1000 });
          return of(null);
        })
      )
      .subscribe()
  }

  private _getAirports() {
    this._airportsService
      .airports()
      .pipe(
        map((airports) => airports.filter((airport) => airport.iata_code)),
        tap((airports: Airport[]) => this.dataSource = [...airports])
      )
      .subscribe()
  }


}
