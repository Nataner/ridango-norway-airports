import {NgModule} from '@angular/core';

import { AirportsComponent } from "./airports.component";
import {AirportsRoutingModule} from "./airports.routing-module";
import {MatTableModule} from "@angular/material/table";
import {HttpClientModule} from "@angular/common/http";
import {AirportsServices} from "./services/airports.services";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {SchedulesService} from "../shared-module/services/schedules.service";
import {MatIconModule} from "@angular/material/icon";
import {SharedModule} from "../shared-module/shared.module";

@NgModule({
    imports: [AirportsRoutingModule, MatTableModule, HttpClientModule, CommonModule, RouterModule, MatIconModule, SharedModule],
  exports: [],
  declarations: [AirportsComponent],
  providers: [AirportsServices, SchedulesService],
})
export class AirportsModule {}
