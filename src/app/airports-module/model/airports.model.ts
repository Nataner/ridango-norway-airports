
export interface Airport {
  lat: number;
  lng: number;
  icao_code: string;
  iata_code: string;
  name: string;
  country_code: string;
}
