import { Component, AfterViewInit, ViewChild, ElementRef, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Location } from "@angular/common";
import { AirportsServices } from "../airports-module/services/airports.services";
import { map, tap } from "rxjs";
import { Airport } from "../airports-module/model/airports.model";

import MapOptions = google.maps.MapOptions;
import MarkerOptions = google.maps.MarkerOptions;
import LatLngLiteral = google.maps.LatLngLiteral;

@Component({
  selector: "google-map-component",
  templateUrl: "google-map.component.html",
  styleUrls: ["google-map.component.scss"]
})
export class GoogleMapComponent implements OnInit,AfterViewInit {
  @ViewChild("mapContainer") google_map!: ElementRef;

  public options: MapOptions = {
    center: { lat: 59.911491, lng: 10.757933 },
    zoom: 4
  }

  public markersOptions: MarkerOptions = {
    draggable: false,
  }

  public markers: LatLngLiteral[] = [];

  constructor(
    private _router: Router,
    private _location: Location,
    private _airportsService: AirportsServices
  ) {}

  ngOnInit() {
    this._getAirports();
  }

  ngAfterViewInit(): void {}

  public goBack = () => {
    this._location.back();
  }

  public goToAirports = () => {
    void this._router.navigate(['/airports'])
  }

  private _getAirports() {
    this._airportsService
      .airports()
      .pipe(
        map((airports) => airports.filter((airport) => airport.iata_code)),
        tap((airports: Airport[]) => this.markers = airports.map((airport) => ({ lat: airport.lat, lng: airport.lng })))
      )
      .subscribe()
  }

}
