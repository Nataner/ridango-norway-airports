import {NgModule} from '@angular/core';

import { GoogleMapComponent } from "./google-map.component";
import { GoogleMapRoutingModule } from "./google-map.routing-module";
import { GoogleMapsModule } from "@angular/google-maps";
import { MatIconModule } from "@angular/material/icon";
import { AirportsServices } from "../airports-module/services/airports.services";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { SharedModule } from "../shared-module/shared.module";


@NgModule({
    imports: [GoogleMapRoutingModule, GoogleMapsModule, MatIconModule, CommonModule, HttpClientModule, SharedModule],
  exports: [],
  declarations: [GoogleMapComponent],
  providers: [AirportsServices],
})
export class GoogleMapModule {
}
